<?php
	abstract class Enum {
		/**
		 * Parse a string value to one of the enum names
		 * @param  string $keyword name to test
		 * @return mixed 			value of name if found, default if not (if set), else false
		 */
		public static function parse($keyword) {
			$constants = static::getConstants(true);
			$default = isset($constants["ENUM_DEFAULT_VALUE"]) ? $constants["ENUM_DEFAULT_VALUE"] : false;
			$caseSensitive = isset($constants["ENUM_CASE_SENSITIVE"]) ? $constants["ENUM_CASE_SENSITIVE"] : true;

			if ($caseSensitive) {
				return isset($constants[$keyword]) ? $constants[$keyword] : $default;
			} else {
				$match = "";
				foreach ($constants as $name => $value) {
					if (strtoupper($name) == strtoupper($keyword)) {
						$match = $value;
					}
				}
				return $match ? $match : $default;
			}
		}

		/**
		 * List all names and values
		 * @param  boolean $include_default wether to include the default value (if set)
		 * @return array                   list of names and values as pairs
		 */
		public static function getConstants($include_default = false) {
			$r = new ReflectionClass(get_called_class());
			$constants = $r->getConstants();

			if (!$include_default and isset($constants["ENUM_DEFAULT_VALUE"])) {
				unset($constants["ENUM_DEFAULT_VALUE"]);
			}

			return $constants;
		}
	}
?>