# Enum (PHP Class)

The Enum class mimics typical enum behaviour as PHP lacks the enum type.

## Usage

Load Enum.php wherever needed. 
	
	<?php
		require_once("Enum.php");
	?>

Create a class that extends upon the Enum class and use class constants to set the names/values.

	<?php
		final class Roles extends Enum {
			const User = 1;
			const Moderator = 2;
			const Admin = 3;
		}
	?>

## Parse

If you want to parse a string value into a correct enum name you can use the static `parse()` function.
	
	<?php
		$input = "User";
	
		switch (Roles::parse($input)) {
			case Roles::User:
				print "User";
				break;
	
			case Roles::Moderator:
				print "Moderator";
				break;
	
			case Roles::Admin:
				print "Admin";
				break;
		}
	?>

The above will print out "User".

## Options

It is possible to enable case-insensitive parsing as well as setting a default value if parsing fails.

	<?php
		final class Roles extends Enum {
			const User = 1;
			const Moderator = 2;
			const Admin = 3;
	
			// Setting for the default value if parsing fails
			const ENUM_DEFAULT_VALUE = self::User;
	
			// Setting for case-insensitive parsing
			const ENUM_CASE_SENSITIVE = false;
		}
	?>